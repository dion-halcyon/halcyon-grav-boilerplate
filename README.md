# Halcyon Boilerplate for Grav
- git clone this repository
- then remove the .git folder after cloning.


# Libraries included:
- bootstrap 4
- slick
- fontawesome 4
- lazysizes
- pure-media <a href="https://github.com/localnetwork/pure-media">instructions here</a>
- quicklink