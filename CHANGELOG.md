# v1.0.0
## 11/07/2019
- improved twig templates
- added quicklink
- added pure-media library
- removed unrelated css.
- adjusted css utilities.