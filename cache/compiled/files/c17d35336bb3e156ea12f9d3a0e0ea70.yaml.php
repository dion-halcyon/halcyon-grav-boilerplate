<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/home/s14host/halcyon-grav-boilerplate/system/blueprints/config/streams.yaml',
    'modified' => 1573137556,
    'data' => [
        'title' => 'PLUGIN_ADMIN.FILE_STREAMS',
        'form' => [
            'validation' => 'loose',
            'hidden' => true,
            'fields' => [
                'schemes.xxx' => [
                    'type' => 'array'
                ]
            ]
        ]
    ]
];
