<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/home/s14host/halcyon-grav-boilerplate/user/plugins/markdown-notices/markdown-notices.yaml',
    'modified' => 1573137397,
    'data' => [
        'enabled' => true,
        'built_in_css' => true,
        'base_classes' => 'notices',
        'level_classes' => [
            0 => 'yellow',
            1 => 'red',
            2 => 'blue',
            3 => 'green'
        ]
    ]
];
