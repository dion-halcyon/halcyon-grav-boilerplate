<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/home/s14host/halcyon-grav-boilerplate/user/plugins/quicklink/languages.yaml',
    'modified' => 1573138383,
    'data' => [
        'en' => [
            'PLUGIN_QUICKLINK' => [
                'TEXT_VARIABLE' => 'Text Variable',
                'TEXT_VARIABLE_HELP' => 'Text to add to the top of a page'
            ]
        ]
    ]
];
